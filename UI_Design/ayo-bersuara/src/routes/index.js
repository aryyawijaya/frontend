
import React from 'react';
import { createBrowserRouter } from "react-router-dom";
import Login from "../Pages/loginComponents/loginPage";
import Dashboard from "../Pages/DashboardComponents/dashBoardPage";
import Signup from "../Pages/signUpComponents/signUpPage";
import LoginRoomUserId from "../Pages/masukRoom2Components/MasukRoom2Page";
import BuatRoom from "../Pages/makeRoomComponents/MakeRoomPage";
import AddFreeRoom from "../Pages/addFreeRoomComponents/AddFreeRoomPage";
import AdminDashboardPage from "../Pages/adminDashboard/AdminDashboardPage";
import AddPremiumRoom from '../Pages/addPremiumRoomComponents/AddPremiumRoomPage';
import LoginRoomEmail from '../Pages/masukRoomEmailComponents/LoginRoomEmail';
import CariRoom from "../Pages/cariRoom/CariRoomPage";
import InputOTPPage from '../Pages/inputOTPPageComponents/InputOTPPage';
import VotingRoom from '../Pages/votingRoomComponents/VotingRoomPage';
import VotingRoomStat from '../Pages/votingRoomWithStatisticComponents/VotingRoomStatPage';
import TransaksiPage from '../Pages/transaksiPageComponents/TransaksiPage';
import LoginByRoomID from '../Pages/masukRoomAsRoomAdmin/LoginbyRoomIDPage';
import EditRoomPage from '../Pages/editRoomComponents/EditRoomPage';
import MyAdminDashboardPage from '../Pages/myAdminDashboardComponents/MyAdminDashboardPage';
import PaymentConfirmPage from '../Pages/paymentConfirmComponents/PaymentConfirmPage';
import LihatDetail from '../Pages/lihatDetaiPaymentComponents/LihatDetailPage';

const routes = createBrowserRouter([
    {
        path: "/",
        element: <Dashboard/>,
    },
    {
        path: "/signup",
        element: <Signup/>,
    },
    {
        path: "/login",
        element: <Login/>,
    },
    {
        path: "/masuk-room-by-userid",
        element: <LoginRoomUserId/>,
    },
    {
        path: "/dashboard",
        element: <Dashboard/>,
    },
    {
        path: "/buat-room",
        element: <BuatRoom/>
    },
    {
        path: "/tambah-free-room",
        element: <AddFreeRoom/>
    },
    {
        path: "/administrator-dashboard",
        element: <AdminDashboardPage/>
    },
    {
        path: "/tambah-premium-room",
        element: <AddPremiumRoom/>
    },
    {
        path: "/masuk-room-by-email",
        element: <LoginRoomEmail/>
    },
    {
        path: "/masuk-room-by-roomid",
        element: <LoginByRoomID/>
    },
    {
        path: "/input-otp",
        element: <InputOTPPage/>
    },
    {
        path: "/voting-room",
        element: <VotingRoom/>
    },
    {
        path: "/voting-statistic",
        element: <VotingRoomStat/>
    },
    {
        path: "/transaksi",
        element: <TransaksiPage/>
    },
    {
        path: "/edit-room",
        element: <EditRoomPage/>
    },
    {
        path: "/my-dashboard",
        element: <MyAdminDashboardPage/>
    },
    {
        path: "/payment-confirmation",
        element: <PaymentConfirmPage/>
    },
    {
        path: "/cari-room",
        element: <CariRoom/>
    },
    {
        path: "/lihat-detail",
        element: <LihatDetail/>
    }
]);

export default routes;