import React from "react";
import { MDBContainer } from "mdbreact";
import { Doughnut } from "react-chartjs-2";

const CreateChart = () => {
    const data = {
        labels: ["Votes"],
        datasets: [{
            label: "Total jumlah vote",
            data: [70, 30],
            backgroundColor: ["#507CB6", "white"],
        }]
    }

    if (typeof data !== "undefined") {
        data.destroy();
    }

    return (
        <MDBContainer>
            <Doughnut data={data} />
        </MDBContainer>   
    )
}

export default CreateChart;