import React from 'react';
import NavigationBar from './NavbarHeader';
import Content from './Content';
import './VotingRoomStatStyle.css';

const VotingRoomStat = () => {
    return (
        <div>
            <div className='headerVotingRoomStat'>
                <NavigationBar/>
            </div>
            <div>
                <Content/>
            </div>
        </div>
    )
}

export default VotingRoomStat;