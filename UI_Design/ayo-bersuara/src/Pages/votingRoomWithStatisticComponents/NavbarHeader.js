import React from 'react';
import './VotingRoomStatStyle.css';
import { Navbar, Container, Nav } from 'react-bootstrap';

const NavigationBar = () => {
    const roomName = localStorage.getItem("roomName");

    return (
        <Navbar>
            <Container>
                <Nav className='navLogoVotingRoomStat'>{ roomName }</Nav>
            </Container>
        </Navbar>
    )
}

export default NavigationBar;