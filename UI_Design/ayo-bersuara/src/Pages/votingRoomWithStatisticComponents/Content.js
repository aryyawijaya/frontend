import React, { useEffect, useState } from 'react';
import './VotingRoomStatStyle.css';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { useNavigate } from "react-router-dom";
import axios from 'axios';

const Content = () => {
    const navigate = useNavigate();

    const [pilihan, setPilihan] = useState([]);
    const [persentase, setPersentase] = useState([]);
    const [stateBaru, setStateBaru] = useState([]);
    const [totalVote, setTotalVote] = useState([]);
    // const [roomId, setRoomId] = useState("");
    const [isOpen, setIsOpen] = useState(false);
    const [voters, setVoters] = useState([]);
    const [premAdminId, setPremAdminId] = useState("");

    async function fetchPilihan(roomId) {
        const result = await axios(
            `http://localhost:8001/pilihanRoom/getPilihanRoom?roomId=${ roomId }`
        );
        // console.log("masuk sini");
        setPilihan(result.data.data);
    }
    async function fetchPersentase(roomId) {
        const result = await axios(
            `http://localhost:8001/vote/count?roomId=${ roomId }`
        );
        // console.log("masuk sini juga");
        setPersentase(result.data.data);
    }
    async function fetchTotalVote(roomId) {
        const result = await axios(
            `http://localhost:8001/vote/counts?roomId=${ roomId }`
        );
        // console.log("masuk sini juga okey");
        setTotalVote(result.data.data);
    }

    const togglePopup = () => {
        setIsOpen(!isOpen);
    };

    const fetchVoters = async (roomId) => {
        const result = await axios.get(
            `http://localhost:8001/voter-room?roomId=${ roomId }`
        );
        setVoters(result.data.data);
    };

    async function apaKek(roomId) {
        try {
            await fetchPilihan(roomId);
            await fetchPersentase(roomId);
            await fetchTotalVote(roomId);
            await fetchVoters(roomId);
            const hasil = persentase.map(pf => {
                // pf.pilihanId
                const p = pilihan.find(p => p.pilihanId === pf.pilihanId);
                return {
                    pilihanId: pf.pilihanId,
                    freq: pf.freq,
                    name: p.name,
                    vision: p.vision,
                    mission: p.mission,
                    photo: p.photo
                    // lanjutin data"nya sesuai response backend
                };
            });
            setStateBaru(hasil);
        } catch (err) {
            console.log(err.response.data.message);
            alert(err.response.data.message);
        }
    }

    useEffect(() => {
        let roomId = localStorage.getItem("roomId");
        const premAdminId = localStorage.getItem("premiumRoomAdminId");
        setPremAdminId(premAdminId);
        if (!roomId) {
            navigate("/cari-room");
            return;
        }
        // ubah PRA#000001 --> PRA%23000001
        roomId = roomId.substring(0, 3) + "%23" + roomId.substring(4, 10);
        // setRoomId(roomId);
        const logged = localStorage.getItem("voterLogged");
        const premAdmin = localStorage.getItem("premiumRoomAdminId");
        const fr = localStorage.getItem("fr");
        if (!logged && !premAdmin && !fr) {
            navigate("/masuk-room-by-email");
            return;
        }
        apaKek(roomId);
    });

    // console.log('pilihan');
    // console.log(pilihan);
    // console.log('persentase');
    // console.log(persentase);
    // console.log('total vote');
    // console.log(totalVote[0].total);

    return (
        <div>
            <div>
                <button
                className='backBtnVotingRoomStat'
                onClick={() => premAdminId ? navigate("/administrator-dashboard") : navigate("/cari-room")
                }>Back</button>
                <button 
                className='listVotersBtnVotingRoomStat'
                onClick={ togglePopup }>List Voters</button>
                {isOpen && (
                    <div className="popup">
                        {voters.map(item => (
                            <ul>
                                <li>{item.voterId} | {item.name}</li>
                            </ul>
                        ))}
                    </div>
                )}
            </div>
            <div>
            </div>
            <Row style={{margin: "0px", padding: "0px"}}>
                {stateBaru.map(item => (
                    <Col>
                        <div className='votingCardBox2'>
                            <div className='votingPhotoBox2'>
                                {!item.photo ? 
                                (<img
                                className='votingImg2'
                                src='../asset/freeRoom.jpg'
                                alt=''
                                ></img>) : 
                                (<img
                                className='votingImg2'
                                src={item.photo}
                                alt=''
                                ></img>)
                                }
                            </div>
                            <div className='votingTextBox2'>
                                <p>{item.name}</p>
                                <p>{item.vision}</p>
                                <p style={{margin: "0px"}}>{item.mission}</p>
                            </div>
                            <div className='boxPercentage'>
                                <h1 className='percentageVote'>{ 
                                parseInt(totalVote[0]?.total) === 0 ?
                                0 :
                                parseInt(item?.freq) / parseInt(totalVote[0]?.total) * 100 
                                }%</h1>
                            </div>
                        </div>
                    </Col>
            ))}
            </Row>
        </div>
    )
}

export default Content;