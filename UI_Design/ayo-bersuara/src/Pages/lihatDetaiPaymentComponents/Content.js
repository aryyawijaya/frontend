import "./LihatDetailStyle.css";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import { Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";

const Content = () => {
    const navigate = useNavigate();

    const [roomName, setRoomName] = useState("");
    const [photo, setPhoto] = useState([]);
    // const [transaksiId, setTransaksiId] = useState("");

    async function fetchData(transaksiId) {
        const result = await axios(
            `http://localhost:8001/transaksi/getById?transaksiId=${ transaksiId }`
        );
        setPhoto(result.data.data);
    }

    useEffect(() => {
        const superAdmin = localStorage.getItem("superAdmin");
        if (!superAdmin) {
            navigate("/");
            return;
        }
        let transaksiId = localStorage.getItem("transaksiId");
        if (!transaksiId) {
            navigate("/my-dashboard");
            return;
        }
        transaksiId = transaksiId.substring(0, 3) + "%23" + transaksiId.substring(4, 10);
        fetchData(transaksiId);
        // setTransaksiId(transaksiId);
        const roomName = localStorage.getItem("roomName");
        setRoomName(roomName);
    }, []);

    console.log(photo);
    console.log(photo.evidence);

    return (
        <div>
            <Button 
            className="backBtnDetail"
            onClick={() => navigate("/my-dashboard")}
            >Back</Button>
            <h1 className="titlePaymentPage">Bukti Pembayaran</h1>
            <Row style={{margin: "0px", padding: "0px"}}>
                <Col>
                    <div className="borderBoxDetail">
                        <p>{roomName}</p>
                    </div>
                </Col>
                <Col>
                    <div className="invinsibleBorderBox">
                        {photo.evidence === null ?
                        <img
                            className="imgDetail"
                            src="/asset/freeRoom.jpg"
                            // src={photo.evidence}
                            alt=""
                        /> :
                        <img
                            className="imgDetail"
                            // src="../asset/freeroom.jpg"
                            src={photo.evidence}
                            alt=""
                        />
                        }
                    </div>
                </Col>
            </Row>
        </div>
    )
}

export default Content;