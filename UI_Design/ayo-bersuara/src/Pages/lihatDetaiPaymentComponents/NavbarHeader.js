import "./LihatDetailStyle.css";
import { Navbar, Container, Nav } from "react-bootstrap";

const NavigationBar = () => {
    return (
        <Navbar variant="dark" className="headerPayment">
            <Container>
                <Nav className="welcomingMyAdmin" style={{marginLeft: "auto"}}>Selamat Datang Admin</Nav>
            </Container>
        </Navbar>
    )
}

export default NavigationBar;