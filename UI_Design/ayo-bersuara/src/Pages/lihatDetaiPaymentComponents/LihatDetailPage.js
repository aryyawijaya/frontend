import "./LihatDetailStyle.css";
import NavigationBar from "./NavbarHeader";
import Content from "./Content";

const LihatDetail = () => {
    return (
        <div>
            <div>
                <NavigationBar/>  
            </div>
            <div>
                <Content/>
            </div>
        </div>
    )
}

export default LihatDetail;