import React, { useEffect, useState } from 'react';
import { Navbar } from "react-bootstrap";
import "./dashBoardStyle.css";
import { useNavigate } from "react-router-dom";

const DashboardNavbar = () => {
    const navigate = useNavigate();
    
    const [premAdminId, setPremAdminId] = useState("");

    useEffect(() => {
        const premAdminId = localStorage.getItem("premiumRoomAdminId");
        setPremAdminId(premAdminId);
    }, []);

    return (
        <Navbar variant="dark" className="header">
            <Navbar.Brand className="navLogoDashboard">AyoBersuara.com</Navbar.Brand>
                <div style={{marginLeft: "auto"}} className="btn-admin">
                    {!premAdminId ? 
                    (<button
                        className="loginButtonDashboard"
                        onClick={() => navigate("/login")}
                        >Login Premium Adminstrator</button>) : 
                    (<><button
                        className="loginButtonDashboard"
                        onClick={() => navigate("/administrator-dashboard")}
                    >Room Administrator Dashboard</button>
                    <button
                        className="logoutButtonDashboard"
                        onClick={() => {
                            localStorage.removeItem("premiumRoomAdminId");
                            window.location.reload();
                        }}
                    >Logout</button></>)}
                </div>
                <div>
                    
                </div>
        </Navbar>
    );
};

export default DashboardNavbar;