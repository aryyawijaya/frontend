import React, { useEffect } from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import "./dashBoardStyle.css";
import { useNavigate } from "react-router-dom";

const Content = () => {
    const navigate = useNavigate();
    useEffect(() => {
        localStorage.removeItem("roomId");
        localStorage.removeItem("roomName");
        localStorage.removeItem("superAdmin");
    }, []);
    return (
        <div>
            <Row style={{padding: "0px", margin: "50px 0px 0px 50px"}}>
                <Col>
                    <h1>Gunakan AyoBersuara untuk memberikan suara anda</h1>
                    <p>Memudahkan anda dalam melakukan voting secara online</p>
                    <div>
                        <button
                        className="signButtonDashboard"
                        onClick={() => navigate("/signup")}
                        >Gabung Premium</button>
                        <button
                        className="makeRoomButton"
                        onClick={() => navigate("/buat-room")}
                        >Buat Room</button>
                    </div>
                    <button
                    className="cariRoomButton"
                    onClick={() => navigate("/cari-room")}
                    >Cari Room</button>
                </Col>
                <Col>
                    <img
                    className="ImgDashboard"
                    src="/asset/img1.png"
                    alt=""/>
                </Col>
            </Row>
            <h1 className="text1Dashboard">Ayo Gabung Premium</h1>
            <h1 className="text2Dashboard">Jadilah premium adminstrator untuk menggunakan fitur premium kami</h1>
            <Row style={{padding: "0px", margin: "50px 50px 0px 50px"}}>
                <Col>
                <img
                className="adsImg"
                src="/asset/premadmin.png"
                alt=""
                style={{marginLeft: "380px"}}/>
                </Col>
                <Col>
                <img
                className="adsImg"
                src="/asset/freeadmin.png"
                alt=""
                style={{marginRight: "380px"}}/>
                </Col>
            </Row>
        </div>
    );
};

export default Content;