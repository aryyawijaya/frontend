import React from 'react';
import './dashBoardStyle.css';
import Navbar from './NavbarHeader';
import Contents from './Content';
import Footer from './Footer';

const Dashboard = () => {
    return (
        <div>
            <div>
                <Navbar/>
            </div>
            <div>
                <Contents/>
            </div>
            <div>
                <Footer/>
            </div>
        </div>
    );
};

export default Dashboard;