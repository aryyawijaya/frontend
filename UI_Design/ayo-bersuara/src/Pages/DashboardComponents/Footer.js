import React from 'react';
import "./dashBoardStyle.css";
import { Navbar, Nav, Button } from "react-bootstrap";
import { useNavigate } from 'react-router-dom';

const Footer = () => {
    const navigate = useNavigate();
    return (
        <Navbar className='footer'>
            <Nav.Link className="footerLink" target="_blank" href="https://gitlab.com/a-squad">Our Gitlab</Nav.Link>
                <div style={{marginLeft: "auto"}} className='hiddenBox'>
                    <Button
                    className='loginAdminBtn'
                    onClick={() => {
                        localStorage.setItem("superAdmin", "ADM#000001");
                        navigate('/my-dashboard');
                    }}
                    >Login Admin</Button>
                </div>
        </Navbar>
    )
}

export default Footer;