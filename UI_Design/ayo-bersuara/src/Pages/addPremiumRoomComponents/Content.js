import React from 'react';
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import "./AddPremiumRoomStyle.css"
import { useState, useEffect } from "react";
import { useNavigate } from "react-router";
import axios from "axios";

const Content = () => {
    const navigate = useNavigate();

    const [premAdminId, setPremAdminId] = useState("");
    const [name, setName] = useState("");
    const [password, setPassword] = useState("");
    const [voterFile, setVoterFile] = useState(null);
    const [pilihanFile, setPilihanFile] = useState(null);
    
    useEffect(() => {
        localStorage.removeItem("roomId");
        localStorage.removeItem("roomName");
        let premAdminId = localStorage.getItem("premiumRoomAdminId");
        console.log(premAdminId);
        if (!premAdminId) {
            navigate("/login");
            return;
        }
        // ubah PRA#000001 --> PRA%23000001
        premAdminId = premAdminId.substring(0, 3) + "%23" + premAdminId.substring(4, 10);
        setPremAdminId(premAdminId);
    }, []);

    const handleChangeName = (e) => {
        setName(e.target.value);
    };

    const handleChangePassword = (e) => {
        setPassword(e.target.value);
    };

    const handleChangeVoterFile = (e) => {
        setVoterFile(e.target.files[0]);
    };

    const handleChangePilihanFile = (e) => {
        setPilihanFile(e.target.files[0]);
    };

    const onSubmitHandler = async (e) => {
        e.preventDefault();
        try {
            const room = await axios.post(
                `http://localhost:8001/room/createPremium?premiumRoomAdminId=${ premAdminId }`,
                { name, password },
                {
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded",
                    },
                }
            );
            let roomId = room.data.data.roomId;
            const roomName = room.data.data.name;
            const transaksiId = room.data.transaksiId;
            localStorage.setItem("roomId", roomId);
            localStorage.setItem("roomName", roomName);
            localStorage.setItem("transaksiId", transaksiId);
            roomId = roomId.substring(0, 3) + "%23" + roomId.substring(4, 10);
            const formData = new FormData();
            formData.append('voters', voterFile);
            await axios.post(
                `http://localhost:8001/room/postVoters?roomId=${ roomId }`,
                formData,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }
            );

            const formDataPilihan = new FormData();
            formDataPilihan.append('pilihan', pilihanFile);
            await axios.post(
                `http://localhost:8001/room/postPilihan?roomId=${ roomId }`,
                formDataPilihan,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }
            );
            alert('success');
            navigate("/transaksi"); // direct ke /upload-bukti
        } catch (err) {
            console.log(err.response.data.message);
            alert(err.response.data.message);
        }

        // axios
        //     .post(
        //         `http://localhost:8001/room/createPremium?premiumRoomAdminId=${ premAdminId }`,
        //         { name, password },
        //         // { name, password, voterFile, pilihanFile }, // FormData
        //         {
        //             headers: {
        //                 "Content-Type": "application/x-www-form-urlencoded",
        //             },
        //         }
        //     )
        //     .then((res) => {
        //         console.log(res);
        //         let roomId = res.data.data.roomId;
        //         roomId = roomId.substring(0, 3) + "%23" + roomId.substring(4, 10);
        //         console.log(roomId);

        //         const formData = new FormData();
        //         formData.append('voters', voterFile);

        //         axios
        //             .post(
        //                 `http://localhost:8001/room/postVoters?roomId=${ roomId }`,
        //                 formData,
        //                 {
        //                     headers: {
        //                         'Content-Type': 'multipart/form-data'
        //                     }
        //                 }
        //             )
        //             .then((res2) => {
        //                 console.log(res2);

        //                 const formDataPilihan = new FormData();
        //                 formDataPilihan.append('pilihan', pilihanFile);

        //                 axios
        //                     .post(
        //                         `http://localhost:8001/room/postPilihan?roomId=${ roomId }`,
        //                         formDataPilihan,
        //                         {
        //                             headers: {
        //                                 'Content-Type': 'multipart/form-data'
        //                             }
        //                         }
        //                     )
        //                     .then((res3) => {
        //                         console.log(res3);
        //                         localStorage.setItem("roomId", res.data.data.roomId);
        //                         localStorage.setItem("roomName", res.data.data.name);
        //                         localStorage.setItem("transaksiId", res.data.transaksiId);
        //                         navigate("/transaksi"); // direct ke /upload-bukti
        //                     })
        //                     .catch((err) => {
        //                         console.log(err.response.data.message);
        //                     });
        //             })
        //             .catch((err) => {
        //                 console.log(err.response.data.message);
        //             });
        //     })
        //     .catch((err) => {
        //         console.log(err.response.data.message);
        //     });
    };

    return (
        <div>
            <button 
            className="backButtonAddRoom"
            onClick={() => navigate("/buat-room")}>Back</button>
            <Row style={{margin: "0px", padding: "0px"}}>
                <Col>
                    <div>
                        <form onSubmit={ onSubmitHandler }>
                            <label className="labelStyleAddRoom">Masukan Nama Room</label>
                            <input
                                className="inputBoxAddPremiumRoom"
                                type="text"
                                name="name"
                                value={name}
                                onChange={ handleChangeName }
                            ></input>
                            <label className="labelStyleAddRoom">Masukan Password</label>
                            <input
                                className="inputBoxAddPremiumRoom"
                                type="text"
                                name="password"
                                value={password}
                                onChange={ handleChangePassword }
                            ></input>
                            <label className="labelStyleAddRoom">Masukan File Voter</label>
                            <input
                                className="fileBoxAddPremiumRoom"
                                type="file"
                                name="voterFile"
                                // value={voterFile}
                                onChange={ handleChangeVoterFile }
                            ></input>
                            <label className="labelStyleAddRoom">Masukan File Pilihan</label>
                            <input
                                className="fileBoxAddPremiumRoom"
                                type="file"
                                name="pilihanFile"
                                // value={pilihanFile}
                                onChange={ handleChangePilihanFile }
                            ></input>
                            <button className="makeNewRoomBtn" type="submit">Buat Room Baru</button>
                        </form>
                    </div>
                </Col>
                <Col>
                    <img className = "imgMakeRoom"
                    src = "/asset/img1.png"
                    alt = ""/>
                </Col>
            </Row>
        </div>
    );
};

export default Content;