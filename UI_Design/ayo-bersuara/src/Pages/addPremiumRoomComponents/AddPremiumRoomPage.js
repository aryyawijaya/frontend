import React from 'react';
import NavigationBar from "./NavbarHeader";
import Content from "./Content";
import "./AddPremiumRoomStyle.css"

const AddPremiumRoom = () => {
    return (
        <div>
            <div className="header">
                <NavigationBar/>
            </div>
            <div>
                <Content/>
            </div>
        </div>
    )
}

export default AddPremiumRoom;