import React from 'react';
import { Navbar, Container } from "react-bootstrap";
import "./AddPremiumRoomStyle.css";

const NavigationBar = () => {
    return (
        <Navbar variant="dark">
            <Container>
                <Navbar.Brand className="navLogo">AyoBersuara.com</Navbar.Brand>
            </Container>
        </Navbar>
    )
}

export default NavigationBar;