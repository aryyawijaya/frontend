import React, { useEffect } from 'react';
import './MakeRoomStyle.css';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import { useNavigate } from 'react-router-dom';

const Content = () => {
    const navigate = useNavigate();
    useEffect(() => {
        localStorage.removeItem("roomId");
        localStorage.removeItem("roomName");
    }, []);
    return (
        <div>
            <button 
            className='backBtnBuatRoom'
            onClick={() => navigate("/dashboard")}>Back</button>
            <h1 className='textBuatRoom'>Pilih Room</h1>
            <Row style={{padding: "0px", margin: "0px"}}>
                <Col className='text-center'>
                    <div className='borderBoxMakeRoom'>
                        <img
                        className='imgLogo'
                        src='/asset/freeLogo.png'
                        alt=''/>
                        <img
                        className='roomImg'
                        src='/asset/freeRoom.jpg'
                        alt=''/>
                        <button 
                        className='pilihBtn'
                        onClick={() => navigate("/tambah-free-room")}>Pilih</button>
                    </div>
                </Col>
                <Col className='text-center'>
                    <div className='borderBoxMakeRoom'>
                        <img
                        className='imgLogo'
                        src='/asset/premLogo.png'
                        alt=''/>
                        <img
                        className='roomImg'
                        src='/asset/premRoom.jpg'
                        alt=''/>
                        <button className='pilihBtn'
                        onClick={() => navigate("/tambah-premium-room")}>Pilih</button>
                    </div>
                </Col>
            </Row>
        </div>
    )
}

export default Content;