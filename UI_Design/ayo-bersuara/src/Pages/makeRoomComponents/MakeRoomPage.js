import React from 'react';
import './MakeRoomStyle.css';
import NavigationBar from './NavbarHeader';
import Content from './Content';

const MakeRoom = () => {
    return (
        <div>
            <div className='headerBuatRoom'>
                <NavigationBar/>
            </div>
            <div>
                <Content/>
            </div>
        </div>
    )
}

export default MakeRoom;