import React from "react";
import './VotingRoomStyle.css';
import NavigationBar from './NavbarHeader';
import Content from './Content';

const VotingRoom = () => {
    return (
        <div>
            <div className='headerVotingRoom'>
                <NavigationBar/>
            </div>
            <div>
                <Content/>
            </div>
        </div>
    )
}

export default VotingRoom;
