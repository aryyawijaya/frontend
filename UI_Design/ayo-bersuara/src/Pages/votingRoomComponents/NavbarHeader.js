import React from "react";
import './VotingRoomStyle.css';
import { Container, Navbar, Nav } from 'react-bootstrap';

const NavigationBar = () => {
    const roomName = localStorage.getItem("roomName");

    return (
        <Navbar>
            <Container>
                <Nav className='navLogoVotingRoom'>{ roomName }</Nav>
            </Container>
        </Navbar>
    )
}

export default NavigationBar;