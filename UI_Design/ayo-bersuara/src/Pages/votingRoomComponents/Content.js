import React, { useEffect, useState } from 'react';
import './VotingRoomStyle.css';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import { useNavigate } from "react-router-dom";
import axios from 'axios';

const Content = () => {
    const navigate = useNavigate();

    const [pilihan, setPilihan] = useState([]);
    const [roomId, setRoomId] = useState("");
    // const [roomIdParsed, setRoomIdParsed] = useState("");
    const [voterId, setVoterId] = useState("");

    async function fetchData(roomIdParsed) {
        const result = await axios(
            `http://localhost:8001/pilihanRoom/getPilihanRoom?roomId=${ roomIdParsed }`
        );
        setPilihan(result.data.data);
    }

    useEffect(() => {
        let roomId = localStorage.getItem("roomId");
        if (!roomId) {
            navigate("/cari-room");
            return;
        }
        // ubah PRA#000001 --> PRA%23000001
        setRoomId(roomId);
        roomId = roomId.substring(0, 3) + "%23" + roomId.substring(4, 10);
        // setRoomIdParsed(roomId);
        const premAdmin = localStorage.getItem("premiumRoomAdminId");
        // const fr = localStorage.getItem("fr");
        if (premAdmin) {
            navigate("/voting-statistic");
            return;
        }
        const logged = localStorage.getItem("voterLogged");
        if (!logged) {
            navigate("/masuk-room-by-email");
            return;
        }
        setVoterId(logged);
        // const isVoted = localStorage.getItem("isVoted");
        // if (isVoted) {
        //     navigate("/voting-statistic");
        //     return;
        // }
        
        // fetchData();
        fetchData(roomId);
    }, []);

    console.log(pilihan);

    const onSubmitHandler = async (pilihanId) => {
        // const voterId = localStorage.getItem("isLoggedInRoom");
        // const roomId = localStorage.getItem("roomId");
        try {
            await axios.post(
                "http://localhost:8001/vote/create",
                { voterId, roomId, pilihanId },
                {
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded",
                    },
                }
            );
            navigate("/voting-statistic");
        } catch (err) {
            console.log(err.response.data.message);
            if (!err.response.data.status) {
                alert(err.response.data.message);
                // window.location.reload();
            }
        }
    };

    return (
        <div>
            <div>
                <button
                className='backBtnVotingRoom'
                onClick={() => navigate("/cari-room")}>Back</button>
                <button
                className="btnVotingStatistic"
                onClick={() => navigate("/voting-statistic")}>Statistic</button>
            </div>
            <div>
                <Row style={{margin: "0px", padding: "0px"}}>
                {pilihan.map(item => (
                        <Col>
                            <div className='votingCardBox'>
                                <div className='votingPhotoBox'>
                                    <img
                                    // style={{width: "100%", height: "auto"}}
                                    className='votingImg'
                                    src={item.photo}
                                    alt=''
                                    ></img>
                                </div>
                                <div className='votingTextBox'>
                                    <p>{item.name}</p>
                                    <p>{item.vision}</p>
                                    <p style={{margin: "0px"}}>{item.mission}</p>
                                </div>
                                <button 
                                className='voteBtn'
                                type="submit"
                                onClick={(e) => {
                                    e.preventDefault();
                                    onSubmitHandler(item.pilihanId);
                                }}
                                >Vote</button>
                            </div>
                        </Col>
                ))}
                </Row>
            </div>
        </div>
    )
}

export default Content;