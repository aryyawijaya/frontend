import React from "react";
import "./LoginRoomEmailStyle.css";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { Button } from "react-bootstrap";

const Content = () => {
    const navigate = useNavigate();
    const [voterEmail, setVoterEmail] = useState("");
    const [roomName, setRoomName] = useState("");

    useEffect(() => {
        const roomId = localStorage.getItem("roomId");
        if (!roomId) {
            navigate("/cari-room");
            return;
        }
        const roomName = localStorage.getItem("roomName");
        setRoomName(roomName);
    }, []);

    const handleChangeVoterEmail = (e) => {
        setVoterEmail(e.target.value);
    };

    const onSubmitHandler = (e) => {
        e.preventDefault();
        navigate("input-otp");
    };

    return (
        <div>
            <Button
            className="backBtnMasukEmail"
            onClick={() => navigate("/cari-room")}>Back</Button>
            <div>
                <h1 className="tagLoginRoomEmail">Masuk Room "{roomName}" dengan email</h1>
            </div>
            <div className="borderInvinsible">
                <from onSubmit={ onSubmitHandler }>
                    <label className="inputBoxLoginRoomEmailLabel" for="voterEmail1">Email</label>
                    <input
                        className="inputBoxLoginRoomEmail"
                        type="text"
                        name="voterEmail"
                        value={voterEmail}
                        id="voterEmail1"
                        onChange={handleChangeVoterEmail}
                    ></input>
                    <button 
                    type="submit"
                    className="sendOTPbyEmailBtn"
                    >Kirim OTP</button>
                </from>
                <div className="borderTop">
                    <button 
                    className="changeToUserIdBtn"
                    onClick={() => navigate("/masuk-room-by-userid")}
                    >Masuk dengan User ID </button>
                </div>
            </div>
        </div>
    )
}

export default Content;