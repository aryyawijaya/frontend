import React from "react";
import "./LoginRoomEmailStyle.css";
import NavigationBar from "./NavbarHeader";
import Content from "./Content";

const LoginRoomEmail = () => {
    return (
        <div>
            <div className="headerLoginRoomEmail">
                <NavigationBar/>
            </div>
            <div>
                <Content/>
            </div>
        </div>
    )
}

export default LoginRoomEmail;