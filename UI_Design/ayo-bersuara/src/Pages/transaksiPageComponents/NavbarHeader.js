import './TransaksiPageStyle.css';
import { Navbar, Container, Nav } from 'react-bootstrap';

const NavigationBar = () => {
    return (
        <Navbar>
            <Container>
                <Navbar.Brand className='navLogoTransaksi'>AyoBersuara.com</Navbar.Brand>
            </Container>
        </Navbar>
    )
}

export default NavigationBar;