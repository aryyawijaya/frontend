import './TransaksiPageStyle.css';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';

const Content = () => {
    const navigate = useNavigate();

    const [transaksiId, setTransaksiId] = useState("");
    const [buktiFile, setBuktiFile] = useState(null);

    useEffect(() => {
        const roomId = localStorage.getItem("roomId");
        if (!roomId) {
            navigate("/buat-room");
            return;
        }
        let transaksiId = localStorage.getItem("transaksiId");
        transaksiId = transaksiId.substring(0, 3) + "%23" + transaksiId.substring(4, 10);
        setTransaksiId(transaksiId);
    }, []);

    const handleChangeBuktiFile = (e) => {
        setBuktiFile(e.target.files[0]);
    };

    const onSubmitHandler = (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append('evidence', buktiFile);
        axios
            .post(
                `http://localhost:8001/transaksi/uploadEvidence?transaksiId=${ transaksiId }`,
                formData,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }
            )
            .then((res) => {
                console.log(res);
                alert('success');
                localStorage.removeItem("transaksiId")
                navigate("/administrator-dashboard");
            })
            .catch((err) => {
                console.log(err.response.data.message);
            });
    };

    return (
        <div>
            <button className='backBtnTransaction'>Back</button>
            <div>
                <Row style={{margin: "0px", padding: "0px"}}>
                    <Col>
                        <img
                            className='pricingImg'
                            src='../asset/pricing.png'
                            alt=''
                        />
                    </Col>
                    <Col>
                        <form onSubmit={ onSubmitHandler }>
                            <div className='detailBox'>
                                <p style={{fontFamily: "sans-serif", fontSize: "20px", fontWeight: "bold"}}>Detail Pembayaran</p>
                                <p style={{fontFamily: "sans-serif", fontSize: "15px"}}>Total pembayaran : Rp. 39.999</p>
                                <p style={{fontFamily: "sans-serif", fontSize: "20Apx", fontWeight: "bold"}}>Transfer Pembayaran</p>
                                <p style={{fontFamily: "sans-serif", fontSize: "15px"}}>2564-3135-6545-6632 (BRI)</p>
                            </div>
                            <div className='uploadBox'>
                                <form>
                                    <label style={{fontFamily: "sans-serif", fontSize: "20px", fontWeight: "bold"}}>Upload Bukti Pembayaran</label>
                                    <input
                                        className="fileBoxTransaction"
                                        type="file"
                                        name="buktiFile"
                                        // value={transactionEvi}
                                        onChange={ handleChangeBuktiFile }
                                    ></input>
                                </form>
                            </div>
                            <div className='infoBox'>
                                <p style={{fontFamily: "sans-serif", fontSize: "20px", fontWeight: "bold"}}>Informasi Penting</p>
                                <p style={{fontFamily: "sans-serif", fontSize: "15px"}}>Waktu operasional mencakup pelayanan konsultasi dan transaksi admin.  Hanya akan dilayani pada hari kerja Senin s/d Jum’at pukul 08:00 WIB - 17:00 WIB </p>
                            </div>
                            <div>
                                <button className="uploadBuktiBayar" type="submit">Upload</button>
                            </div>
                        </form>
                    </Col>
                </Row>
            </div>
        </div>
    )
}

export default Content;