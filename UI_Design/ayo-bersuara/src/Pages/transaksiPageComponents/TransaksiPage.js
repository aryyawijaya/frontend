import './TransaksiPageStyle.css';
import NavigationBar from './NavbarHeader';
import Content from './Content';

const TransaksiPage = () => {
    return (
        <div>
            <div className='headerTransaksiPage'>
                <NavigationBar/>
            </div>
            <div>
                <Content/>
            </div>
        </div>
    )
}

export default TransaksiPage;