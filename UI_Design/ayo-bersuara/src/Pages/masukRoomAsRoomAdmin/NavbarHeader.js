import './LoginbyRoomIDStyle.css';
import { Navbar, Container, Nav } from 'react-bootstrap';

const NavigationBar = () => {
    return (
        <Navbar>
            <Container>
                <Navbar.Brand className='navLogoLoginRoomId'>AyoBersuara.com</Navbar.Brand>
            </Container>
        </Navbar>
    )
}

export default NavigationBar;