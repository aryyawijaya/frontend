import './LoginbyRoomIDStyle.css';
import NavigationBar from './NavbarHeader';
import Content from './Content';

const LoginByRoomID = () => {
    return (
        <div>
            <div className='headerRoomID'>
                <NavigationBar/>
            </div>
            <div>
                <Content/>
            </div>
        </div>
    )
}

export default LoginByRoomID;