import './LoginbyRoomIDStyle.css';
import { useEffect, useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { Button } from 'react-bootstrap';

const Content = () => {
    const navigate = useNavigate();

    const [roomId, setRoomId] = useState("");
    const [password, setPassword] = useState("");

    const handleChangeRoomId = (e) => {
        setRoomId(e.target.value);
    };

    const handleChangePassword = (e) => {
        setPassword(e.target.value);
    };

    useEffect(() => {
        localStorage.removeItem("roomId");
        localStorage.removeItem("roomName");
    }, []);

    const onSubmitHandler = (e) => {
        e.preventDefault();
        axios
            .post(
                `http://localhost:8001/room/enter`,
                { roomId, password },
                {
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded",
                    },
                }
            )
            .then((res) => {
                console.log(res);
                localStorage.setItem("roomId", res.data.data.roomId);
                localStorage.setItem("roomName", res.data.data.name);
                localStorage.setItem("fr", "fr");
                navigate('/voting-statistic');
            })
            .catch((err) => {
                console.log(err.response.data.message);
                alert(err.response.data.message);
            });
    }

    return (
        <div>
            <div>
            <Button
            className='backBtnCariRoom'
            onClick={() => navigate("/cari-room")}>Back</Button>
            </div>
            <h1 className='textRoomID'>Masuk Room Dengan RoomID</h1>
            <div className='borderInvinsible4'>
                <form onSubmit={ onSubmitHandler }>
                    <label className='labelLoginRoomId'>Room ID</label>
                    <input
                        className='inputBoxLoginRoomId'
                        type="text"
                        name="roomId"
                        value={roomId}
                        onChange={ handleChangeRoomId }
                    ></input>
                    <label className='labelLoginRoomId'>Password</label>
                    <input
                        className='inputBoxLoginRoomId'
                        type="text"
                        name="password"
                        value={password}
                        onChange={ handleChangePassword }
                    ></input>
                    <button type="submit" className="joinButtonRoomID">Masuk Room</button>
                </form>
            </div>
        </div>
    )
}

export default Content;