import React from 'react';
import "./MyAdminDashboardStyle.css";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { useEffect, useState } from "react";
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

const Content = () => {
    const navigate = useNavigate();

    const [rooms, setRooms] = useState([]);

    useEffect(() => {
        const superAdmin = localStorage.getItem("superAdmin");
        if (!superAdmin) {
            navigate("/");
            return;
        }
        localStorage.removeItem("transaksiId");
        localStorage.removeItem("roomId");
        localStorage.removeItem("roomName");
        async function fetchData() {
            const result = await axios(
                `http://localhost:8001/room/getPremiumRooms`
            );
            setRooms(result.data.data);
        }
        fetchData();
    }, []);

    console.log(rooms);

    useEffect(() => {
        async function fetchData() {
            localStorage.removeItem("roomId");
            localStorage.removeItem("roomName");
            const result = await axios(
                `http://localhost:8001/room/getPremiumRooms`
            );
            setRooms(result.data.data);
        }
        fetchData();
    }, []);

    console.log(rooms);
    return (
        <div>
            <Button
            className='backDashboard'
            onClick={() => navigate("/dashboard")}
            >Dashboard</Button>
            <div>
                <h1 className='myAdminPageTag'>Dashboard Admin</h1>
            </div>
            <Row style={{padding: "0px", margin: "0px"}}>
                <Col className='col-3'>
                    <div className='filterBoxMyAdmin'>
                        <p style={{fontWeight: "bold"}}>List Room</p>
                        <div className='form-check'>
                            <input
                                className='form-check-input'
                                type='radio'
                                name='roomFilterMyAdmin'
                                id='roomFilterMyAdmin1'
                            ></input>
                            <label className="form-check-label" for="roomFilterMyAdmin1">Belum di Konfirmasi</label>
                        </div>
                        <div className='form-check'>
                            <input
                                className='form-check-input'
                                type='radio'
                                name='roomFilterMyAdmin'
                                id='roomFilterMyAdmin2'
                            ></input>
                            <label className="form-check-label" for="roomFilterMyAdmin2">Sudah di Konfirmasi</label>
                        </div>
                    </div>
                </Col>
                <Col>
                    <div>
                        {rooms.map(item => (
                            <Card className='borderBoxMyAdmin' key={item.roomId}>
                                <Card.Body>
                                    <Row style={{padding: "0px", margin: "0px"}}>
                                        <Col className='col-9'>
                                            <Card.Title>{item.name}</Card.Title>
                                            {/* <Card.Text>Jumlah Pilihan : {item.jmlPilihan}</Card.Text>
                                            <Card.Text>Jumlah Voters  : {item.jmlVoters}</Card.Text>
                                            <Card.Text>Jumlah Vote    : {item.jmlVote}</Card.Text> */}
                                        </Col>
                                        <Col style={{padding: "30px"}}>
                                            <div>
                                                {
                                                    item.paymentStatus === 0 ?
                                                    <Button
                                                    className='konfirmasiBtn'
                                                    onClick={() => {
                                                        localStorage.setItem("transaksiId", item.transaksiId);
                                                        localStorage.setItem("roomId", item.roomId);
                                                        localStorage.setItem("roomName", item.name);
                                                        navigate("/payment-confirmation");
                                                    }}
                                                    >Konfirmasi</Button> :
                                                    <Button
                                                    className='lihatBuktiBtn'
                                                    onClick={() => {
                                                        localStorage.setItem("transaksiId", item.transaksiId);
                                                        localStorage.setItem("roomId", item.roomId);
                                                        localStorage.setItem("roomName", item.name);
                                                        navigate("/lihat-detail");
                                                    }}
                                                    >Lihat Bukti</Button>
                                                }
                                            </div>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        ))}
                    </div>
                </Col>
            </Row>
        </div>
    )
}

export default Content;