import "./MyAdminDashboardStyle.css";
import NavigationBar from "./NavbarHeader";
import Content from "./Content";

const MyAdminDashboardPage = () => {
    return (
        <div>
            <div>
                <NavigationBar/>
            </div>
            <div>
                <Content/>
            </div>
        </div>
    )
}

export default MyAdminDashboardPage;