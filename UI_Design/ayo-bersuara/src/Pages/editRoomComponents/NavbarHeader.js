import './EditRoomStyle.css'
import { Navbar, Container, Nav } from 'react-bootstrap'

const NavigationBar = () => {
    return (
        <Navbar>
            <Container>
                <Nav className='pageTitle'>Edit Premium Room</Nav>
            </Container>
        </Navbar>
    )
}

export default NavigationBar;