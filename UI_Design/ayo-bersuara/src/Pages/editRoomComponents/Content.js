import './EditRoomStyle.css';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { useEffect, useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

const Content = () => {
    const [name, setName] = useState("");
    const [password, setPassword] = useState("");
    const [roomId, setRoomId] = useState("");

    const navigate = useNavigate();

    useEffect(() => {
        let roomId = localStorage.getItem("roomId");
        if (!roomId) {
            navigate("/administrator-dashboard");
            return;
        }
        roomId = roomId.substring(0, 3) + "%23" + roomId.substring(4, 10);
        setRoomId(roomId);
    }, []);

    const handleChangeName = (e) => {
        setName(e.target.value);
    };

    const handleChangePassword = (e) => {
        setPassword(e.target.value);
    };

    const onSubmitHandler = (e) => {
        e.preventDefault();
        axios
            .put(
                `http://localhost:8001/room/editRoom?roomId=${ roomId }`,
                { name, password },
                {
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded",
                    },
                }
            )
            .then((res) => {
                console.log(res);
                localStorage.removeItem("roomId");
                localStorage.removeItem("roomName");
                navigate("/administrator-dashboard");
            })
            .catch((err) => {
                alert(err.response.data.message);
                console.log(err.response.data.message);
            });
    };

    return (
        <div>
            <button 
            className="backButtonEditRoom"
            onClick={() => navigate("/administrator-dashboard")}
            >Back</button>
            <Row style={{margin: "0px", padding: "0px"}}>
                <Col>
                    <div>
                        <form onSubmit={ onSubmitHandler }>
                            <label className="labelStyleEditRoom">Masukan Nama Room Baru</label>
                            <input
                                className='inputBoxEditRoom'
                                type='text'
                                name='name'
                                value={name}
                                onChange={handleChangeName}
                            ></input>
                            <label className="labelStyleEditRoom">Masukan Password Baru</label>
                            <input
                                className="inputBoxEditRoom"
                                type="text"
                                name="password"
                                value={password}
                                onChange={ handleChangePassword }
                            ></input>
                            <button className='doneEditingBtn' type='submit'>Save Room</button>
                        </form>
                    </div>
                </Col>
                <Col>
                    <img className = "imgEditRoom"
                    src = "/asset/img1.png"
                    alt = ""/>               
                </Col>
            </Row>
        </div>
    )
}

export default Content;