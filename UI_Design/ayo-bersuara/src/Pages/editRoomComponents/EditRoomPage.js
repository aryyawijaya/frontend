import './EditRoomStyle.css';
import NavigationBar from './NavbarHeader';
import Content from './Content';

const EditRoomPage = () => {
    return (
        <div>
            <div className='headerEditRoom'>
                <NavigationBar/>
            </div>
            <div>
                <Content/>
            </div>
        </div>
    )
}

export default EditRoomPage;