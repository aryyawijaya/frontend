import React from 'react';
import { Container, Navbar } from "react-bootstrap";
import "./SignUpStyle.css";

const NavigationBar = () => {
    return (
        <Navbar variant="dark">
            <Container>
                <Navbar.Brand className="navLogo">AyoBersuara.com</Navbar.Brand>
            </Container>
        </Navbar>
    );
};

export default NavigationBar;