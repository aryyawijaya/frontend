import React, { useEffect } from 'react';
import { useState } from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import './SignUpStyle.css';
import { useNavigate } from "react-router-dom";
import axios from "axios";

const Contents = () => {
    const [email, setEmail] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    useEffect(() => {
        localStorage.removeItem("roomId");
        localStorage.removeItem("roomName");
    }, []);

    const handleChangeEmail = (e) => {
        setEmail(e.target.value);
    };

    const handleChangeUsername = (e) => {
        setUsername(e.target.value);
    };

    const handleChangePassword = (e) => {
        setPassword(e.target.value);
    };

    const onSubmitHandler = (e) => {
        e.preventDefault();
        axios
            .post(
                "http://localhost:8001/premiumRoomAdmin/create",
                { email, username, password },
                {
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded",
                    },
                }
            )
            .then((res) => {
                // perlu store token
                console.log(res);
                const id = res.data.data.premiumRoomAdminId;
                console.log(id);
                localStorage.setItem("premiumRoomAdminId", id);
                navigate("/administrator-dashboard");
            })
            .catch((err) => {
                console.log(err.response.data.message);
            });
    };

    const navigate = useNavigate();
    return (
        <div>
            <button 
            className = "backButtonSignup" 
            onClick={() => navigate("/dashboard")}>Back</button>
            <Row style={{padding: "0px", margin: "0px"}}>
                <Col>
                <div className="d-flex text-center">
                    <h1>Ayo Bergabung Dari Sekarang Untuk Memulai Berlangganan</h1>
                </div>
                <img className = "imgSignup"
                src = "/asset/img1.png"
                alt = ""/>
                </Col>
                <Col>
                <div>
                    <form onSubmit={ onSubmitHandler }>
                        <label className = "labelStyleSignup">Email</label>
                        <input 
                            className = "inputBoxSignup"
                            type = "text"
                            name = "email"
                            value = {email}
                            onChange = { handleChangeEmail }
                        ></input>
                        <label className = "labelStyleSignup">Username</label>
                        <input 
                            className = "inputBoxSignup"
                            type = "text"
                            name = "username"
                            value = {username}
                            onChange = { handleChangeUsername }
                        ></input>
                        <label className = "labelStyleSignup">Password</label>
                        <input
                            className = "inputBoxSignup"
                            type = "text"
                            name = "password"
                            value = {password}
                            onChange = { handleChangePassword }          
                        ></input>
                        <button type="submit" className = "signUpButtonSignup">Sign up</button>
                    </form>
                </div>
                </Col>
            </Row>
        </div>
    );
};

export default Contents