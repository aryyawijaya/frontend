import React from 'react';
import './SignUpStyle.css';
import NavigationBar from './NavbarHeader';
import Contents from './Content';

const SignUp = () => {
    return (
        <div>
            <div className = "header">
            <NavigationBar/>
            </div>
            <div>
                <Contents/>
            </div>
        </div>
    );
};

export default SignUp;