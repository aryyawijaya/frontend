import React from 'react';
import "./AdminDashboardStyle.css";
import { Navbar, Container, Nav } from "react-bootstrap";

const NavigationBar = () => {
    return (
        <Navbar variant="dark" className="header">
            <Container>
                <Nav className="welcomingAdmin" style={{marginLeft: "auto"}}>Selamat Datang Administrator</Nav>
            </Container>
        </Navbar>
    )
}

export default NavigationBar;