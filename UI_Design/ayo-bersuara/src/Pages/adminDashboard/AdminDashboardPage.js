import React from 'react';
import "./AdminDashboardStyle.css";
import NavigationBar from "./NavbarHeader";
import Content from "./Content";

const AdminDashboardPage = () => {
    return (
        <div>
            <div>
                <NavigationBar/>
            </div>
            <div>
                <Content/>
            </div>
        </div>
    )
}

export default AdminDashboardPage;