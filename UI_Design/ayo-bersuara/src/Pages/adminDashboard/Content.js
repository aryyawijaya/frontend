import React from 'react';
import "./AdminDashboardStyle.css";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from 'axios';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

const Content = () => {
    const [rooms, setRooms] = useState([]);

    const navigate = useNavigate();

    useEffect(() => {
        localStorage.removeItem("roomId");
        localStorage.removeItem("roomName");
        let id = localStorage.getItem("premiumRoomAdminId");
        if (!id) {
            navigate("/dashboard");
            return;
        }
        // ubah PRA#000001 --> PRA%23000001
        console.log(id);
        id = id.substring(0, 3) + "%23" + id.substring(4, 10);
        async function fetchData() {
            const result = await axios(
                `http://localhost:8001/premiumRoomAdmin/getAllRoom?premiumRoomAdminId=${ id }`
            );
            setRooms(result.data.data);
        }
        fetchData();
    }, []);

    console.log(rooms);
    
    return (
        <div>
            <Button
            className='backBtnAdminDashboard'
            onClick={() => navigate("/dashboard")}>Back</Button>
            <div>
                <h1 className="pageTag">Dashboard Adminstrator</h1>
            </div>
            <Row style={{padding: "0px", margin: "0px"}}>
                <Col className="col-3">
                    <div className="filterBox">
                        <p style={{fontWeight: "bold"}}>Room Saya</p>
                        <div className="form-check">
                            <input
                                className="form-check-input"
                                type="radio"
                                name="roomFilterAdmin"
                                id="roomFilterAdmin1"
                            ></input>
                            <label className="form-check-label" for="roomFilterAdmin1"
                            >Premium Room</label>
                        </div>
                        <div className="form-check">
                            <input
                                className="form-check-input"
                                type="radio"
                                name="roomFilterAdmin"
                                id="roomFilterAdmin2"
                                checked
                            ></input>
                            <label className="form-check-label" for="roomFilterAdmin2"
                            >Free Room</label>
                        </div>
                    </div>
                </Col>
                <Col className="col">
                    <Row>
                        <button 
                        className="makeRoomAdminDashboard"
                        onClick={() => navigate("/buat-room")}
                        >Buat Room</button>
                    </Row>
                    <Row>
                        <div>
                            {rooms.map(item => (
                                <Card className="borderBoxDashboardAdmin" key={item.roomId}>
                                <Card.Body>
                                    <Row>
                                        <Col className='col-9'>
                                            <Card.Title>{item.name}</Card.Title>
                                            <Card.Subtitle className="mb-2">{item.roomId}</Card.Subtitle>
                                        </Col>
                                        <Col>
                                            <div className='buttonAlignBox'>
                                                <Row style={{margin: "auto"}}>
                                                    <Col style={{padding: "0px"}}>
                                                        <Button
                                                        className="lihatDetailBtn"
                                                        onClick={() => {
                                                            localStorage.setItem("roomId", item.roomId);
                                                            localStorage.setItem("roomName", item.name);
                                                            navigate("/voting-room");
                                                        }} // ke /room 
                                                        >Lihat</Button>
                                                    </Col>
                                                    <Col>
                                                        <Button
                                                        className="editBtn"
                                                        onClick={() => {
                                                            localStorage.setItem("roomId", item.roomId);
                                                            localStorage.setItem("roomName", item.name);
                                                            navigate("/edit-room");
                                                        }} // ke /edit-room 
                                                        >Edit</Button>
                                                    </Col>
                                                </Row>
                                                <Row style={{margin: "auto"}}>
                                                    <Col style={{padding: "0px"}}>
                                                        <Button
                                                        className="qrBtn"
                                                        onClick={() => navigate("/administrator-dashboard")}
                                                        >Unduh QR</Button>
                                                    </Col>
                                                    <Col>
                                                        <Button
                                                        className="deleteButton"
                                                        onClick={async () => {
                                                            let roomId = item.roomId;
                                                            roomId = roomId.substring(0, 3) + "%23" + roomId.substring(4, 10);
                                                            if (window.confirm("Apakah Anda yakin mengahpus room ini?")) {
                                                                await axios.delete(
                                                                    `http://localhost:8001/room?roomId=${ roomId }`
                                                                )
                                                                window.location.reload();
                                                            }
                                                        }}
                                                        >Delete</Button>
                                                    </Col>
                                                </Row>
                                            </div>
                                        </Col>
                                    </Row>
                                </Card.Body>
                                </Card>
                            ))}
                        </div>
                    </Row>
                </Col>
            </Row>
            
        </div>
    );
};

export default Content;