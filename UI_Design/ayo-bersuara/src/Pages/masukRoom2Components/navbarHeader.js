import React from 'react';
import { Container, Navbar } from "react-bootstrap";
import "./masukRoom2style.css";

const NavigationBar = () => {
    return (
        <Navbar>
            <Container>
                <Navbar.Brand className="navLogo">AyoBersuara.com</Navbar.Brand>
            </Container>
        </Navbar>
    );
};

export default NavigationBar;