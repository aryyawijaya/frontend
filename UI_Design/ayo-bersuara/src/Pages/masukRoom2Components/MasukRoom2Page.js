import React from 'react';
import './masukRoom2style.css';
import NavigationBar from './navbarHeader';
import Content from './Content';

const MasukRoom2 = () => {
    return (
        <div>
            <div className = "Header">
                <NavigationBar/>
            </div>
            <div>
                <Content/>
            </div>
        </div>
    );
};

export default MasukRoom2;