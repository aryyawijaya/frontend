import React, { useEffect } from 'react';
import { useState } from "react";
import "./masukRoom2style.css";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { Button } from 'react-bootstrap';

const Content = () => {
    const navigate = useNavigate();

    const [voterId, setVoterId] = useState("");
    const [password, setPassword] = useState("");
    const [roomId, setRoomId] = useState("");
    const [roomName, setRoomName] = useState("");

    useEffect(() => {
        let roomId = localStorage.getItem("roomId");
        if (!roomId) {
            navigate("/cari-room");
            return;
        }
        const roomName = localStorage.getItem("roomName");
        setRoomName(roomName);
        // ubah PRA#000001 --> PRA%23000001
        roomId = roomId.substring(0, 3) + "%23" + roomId.substring(4, 10);
        setRoomId(roomId);
    }, []);

    const handleChangeVoterId = (e) => {
        setVoterId(e.target.value);
    };

    const handleChangePassword = (e) => {
        setPassword(e.target.value);
    };

    const onSubmitHandler = (e) => {
        e.preventDefault();
        console.log(roomId);
        axios
            .post(
                `http://localhost:8001/voter/loginById?roomId=${ roomId }`,
                { voterId, password },
                {
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded",
                    },
                }
            )
            .then((res) => {
                console.log(res);
                const id = res.data.voter.voterId;
                // console.log(id);
                localStorage.setItem("voterLogged", id);
                navigate("/voting-room"); // ke page room
            })
            .catch((err) => {
                console.log(err.response.data.message);
                alert(err.response.data.message);
            });
    };

    return (
        <div>
            <Button
            className='backBtnMasukUserID'
            onClick={() => navigate("/cari-room")}>Back</Button>
            <h1 className="text1">Masuk Room "{ roomName }" Dengan voter ID</h1>
            <div className='borderInvinsible3'>
                <form onSubmit={ onSubmitHandler }>
                    <label className="labelStyleLoginUserId">voter ID</label>
                    <input
                        className="inputBoxLoginUserId"
                        type="text"
                        name="voterId"
                        value={voterId}
                        onChange={ handleChangeVoterId }
                    ></input>
                    <label className="labelStyleLoginUserId">Password</label>
                    <input
                        className="inputBoxLoginUserId"
                        type="text"
                        name="password"
                        value={password}
                        onChange={ handleChangePassword }
                    ></input>
                    <button type="submit" className="joinButton">Masuk Room</button>
                </form>
                <div className='borderTop'>
                    <button 
                    className='changeToEmailBtn'
                    onClick={() => navigate("/masuk-room-by-email")}
                    >Masuk Dengan Email</button>
                </div>
            </div>
        </div>
    )
}

export default Content;