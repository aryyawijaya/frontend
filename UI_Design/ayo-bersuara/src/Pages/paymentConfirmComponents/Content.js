import "./PaymentConfirmStyle.css";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { useNavigate } from "react-router-dom";
import { Button } from "react-bootstrap";
import { useEffect, useState } from "react";
import axios from "axios";

const Content = () => {
    const navigate = useNavigate();

    const [roomName, setRoomName] = useState("");
    const [transaksiId, setTransaksiId] = useState("");
    const [administratorId, setAdministratorId] = useState("");
    const [photo, setPhoto] = useState([]);

    async function fetchData(transaksiId) {
        const result = await axios(
            `http://localhost:8001/transaksi/getById?transaksiId=${ transaksiId }`
        );
        setPhoto(result.data.data);
    }
    
    useEffect(() => {
        const superAdmin = localStorage.getItem("superAdmin");
        if (!superAdmin) {
            navigate("/");
            return;
        }
        let transaksiId = localStorage.getItem("transaksiId");
        if (!transaksiId) {
            navigate("/my-dashboard");
            return;
        }
        transaksiId = transaksiId.substring(0, 3) + "%23" + transaksiId.substring(4, 10);
        setTransaksiId(transaksiId);
        let administratorId = localStorage.getItem("superAdmin");
        administratorId = administratorId.substring(0, 3) + "%23" + administratorId.substring(4, 10);
        setAdministratorId(administratorId);
        const roomName = localStorage.getItem("roomName");
        setRoomName(roomName);
        
        fetchData(transaksiId);
    }, []);

    const onSubmitHandler = (e) => {
        e.preventDefault();
        let transaksiId = localStorage.getItem("transaksiId");
        axios
            .put(
                `http://localhost:8001/administrator/validateTransaction?administratorId=${ administratorId }`,
                { transaksiId },
                {
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded",
                    },
                }
            )
            .then((res) => {
                console.log(res);
                navigate("/my-dashboard");
            })
            .catch((err) => {
                console.log(err.response.data.message);
            });
    }

    console.log(photo);
    console.log(photo.evidence);

    return (
        <div>
            <Button 
            className="backBtnPayment"
            onClick={() => navigate("/my-dashboard")}
            >Back</Button>
            <h1 className="titlePaymentPage">Konfirmasi Pembayaran</h1>
            <Row style={{margin: "0px", padding: "0px"}}>
                <Col>
                    <div className="borderBoxPayment">
                        <p>{roomName}</p>
                    </div>
                </Col>
                <Col>
                    <form onSubmit={ onSubmitHandler }>
                        <div className="invinsibleBorderBox">
                        {photo.evidence === null ?
                            <img
                                className="imgDetail"
                                src="/asset/freeRoom.jpg"
                                // src={photo.evidence}
                                alt=""
                            /> :
                            <img
                                className="imgDetail"
                                // src="../asset/freeroom.jpg"
                                src={photo.evidence}
                                alt=""
                            />
                            }
                            <div>
                                <Button
                                className="confirmPayBtn"
                                type="submit"
                                >Konfirmasi Pembayaran</Button>
                            </div>
                        </div>
                    </form>
                </Col>
            </Row>
        </div>
    )
}

export default Content;