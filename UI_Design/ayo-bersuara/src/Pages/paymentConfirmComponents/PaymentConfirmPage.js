import "./PaymentConfirmStyle.css";
import NavigationBar from "./NavbarHeader";
import Content from "./Content";

const PaymentConfirmPage = () => {
    return (
        <div>
            <div>
                <NavigationBar/>
            </div>
            <div>
                <Content/>
            </div>
        </div>
    )
}

export default PaymentConfirmPage;