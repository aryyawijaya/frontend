import React from 'react';
import { Navbar } from "react-bootstrap";
import { useNavigate } from 'react-router-dom';
import "./CariRoomStyle.css";

const DashboardNavbar = () => {
    const navigate = useNavigate();

    return (
        <Navbar variant="dark" className="header">
            <Navbar.Brand className="navLogoCariRoom">AyoBersuara.com</Navbar.Brand>
            <div style={{marginLeft: "auto"}} className="btn-admin">
                <button
                    className="masukRoomAdmin"
                    onClick={() => navigate("/masuk-room-by-roomid")}
                    >Masuk Sebagai Room Admin</button>
            </div>
        </Navbar>
    );
};

export default DashboardNavbar;