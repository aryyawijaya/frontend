import React from 'react';
import "./CariRoomStyle.css";
import NavigationBar from "./NavbarHeader";
import Content from "./Content";

const CariRoomPage = () => {
    return (
        <div>
            <div>
                <NavigationBar/>
            </div>
            <div>
                <Content/>
            </div>
        </div>
    );
};

export default CariRoomPage;