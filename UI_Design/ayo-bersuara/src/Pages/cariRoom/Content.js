import React from 'react';
import "./CariRoomStyle.css";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from 'axios';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

const Content = () => {
    const [rooms, setRooms] = useState([]);
    const [premAdminId, setPremAdminId] = useState("");

    const navigate = useNavigate();
    
    async function fetchData() {
        const result = await axios(`http://localhost:8001/room/getAll`);
        setRooms(result.data.data);
    }

    useEffect(() => {
        const premAdminId = localStorage.getItem("premiumRoomAdminId");
        setPremAdminId(premAdminId);
        localStorage.removeItem("fr");
        localStorage.removeItem("roomId");
        localStorage.removeItem("roomName");
        localStorage.removeItem("voterLogged");
        fetchData();
    }, []);

    // console.log(rooms);

    return (
        <div>
            <div>
            <Button
            className='backBtnCariRoom'
            onClick={() => navigate("/dashboard")}>Back</Button>
            </div>
            <div>
                <label className="pageTag">Nama Room</label>
                <input
                    className="inputBoxSearch"
                    type="text"
                    name="search"
                    // value={""}
                    // onChange={  }
                ></input>
            </div>
            <Row style={{padding: "0px", margin: "0px"}}>
                <Col className="col-3">
                    <div className="filterBox">
                        <p style={{fontWeight: "bold"}}>Jenis Room</p>
                        <div className="form-check">
                            <input
                                className="form-check-input"
                                type="radio"
                                name="roomFilterAdmin"
                                id="roomFilterAdmin1"
                            ></input>
                            <label className="form-check-label" for="roomFilterAdmin1"
                            >Premium Room</label>
                        </div>
                        <div className="form-check">
                            <input
                                className="form-check-input"
                                type="radio"
                                name="roomFilterAdmin"
                                id="roomFilterAdmin2"
                                checked
                            ></input>
                            <label className="form-check-label" for="roomFilterAdmin2"
                            >Free Room</label>
                        </div>
                    </div>
                </Col>
                <Col className="col">
                    <Row>
                        <div>
                            {rooms.map(item => (
                                <Card className="borderBoxCariRoom" key={item.roomId}>
                                <Card.Body>
                                    <Row>
                                        <Col className='col-9'>
                                            <Card.Title>{item.name}</Card.Title>
                                            <Card.Subtitle className="mb-2">{item.roomId}</Card.Subtitle>
                                        </Col>
                                        <Col>
                                            <div className='buttonAlignBox'>
                                                <Row style={{margin: "auto"}}>
                                                    <Col style={{padding: "30px"}}>
                                                        <Button
                                                        className="masukBtn"
                                                        onClick={() => {
                                                            if (premAdminId === null) {
                                                                localStorage.setItem("roomId", item.roomId);
                                                                localStorage.setItem("roomName", item.name);
                                                                navigate("/voting-room");
                                                            } else {
                                                                if (item.premiumRoomAdminId === null) {
                                                                    localStorage.setItem("roomId", item.roomId);
                                                                    localStorage.setItem("roomName", item.name);
                                                                    navigate("/voting-room");
                                                                } else if (item.premiumRoomAdminId === premAdminId) {
                                                                    localStorage.setItem("roomId", item.roomId);
                                                                    localStorage.setItem("roomName", item.name);
                                                                    navigate("/voting-room");
                                                                } else {
                                                                    alert("you don't have an access");
                                                                    navigate('/cari-room');
                                                                }
                                                            }
                                                        }} // ke /room 
                                                        >Masuk</Button>
                                                    </Col>
                                                </Row>
                                            </div>
                                        </Col>
                                    </Row>
                                </Card.Body>
                                </Card>
                            ))}
                        </div>
                    </Row>
                </Col>
            </Row>
            
        </div>
    );
};

export default Content;