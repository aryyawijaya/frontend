import React from "react";
import "./InputOTPStyle.css";
import NavigationBar from "./NavbarHeader";
import Content from "./Content";

const InputOTPPage = () => {
    return (
        <div>
            <div className="headerInputOTP">
                <NavigationBar/>
            </div>
            <div>
                <Content/>
            </div>
        </div>
    )
}

export default InputOTPPage;