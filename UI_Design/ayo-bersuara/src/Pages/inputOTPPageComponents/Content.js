import React from "react";
import { useState } from "react";
import { Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import "./InputOTPStyle.css";

const Content = () => {
    const [inputOTP, setInputOTP] = useState("")
    
    const navigate = useNavigate()
    return (
        <div>
            <Button
            className="backBtnInputOTP"
            onClick={() => navigate("/masuk-room-by-email")}>Back</Button>
            <div>
                <h1 className="tagInputOTPPage">Masukan OTP Yang Kami Kirimkan Ke Email Anda</h1>
            </div>
            <div className="borderInvinsible2">
                <form>
                    <input
                        className="inputBoxOTP"
                        type="text"
                        name="inputOTP"
                        value={inputOTP}
                    ></input>
                    <div>
                        <button className="inputOTPBtn">Masuk Room</button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default Content;