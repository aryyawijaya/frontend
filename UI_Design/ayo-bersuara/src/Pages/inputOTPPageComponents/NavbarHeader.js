import React from "react";
import "./InputOTPStyle.css";
import { Container, Navbar } from "react-bootstrap";

const NavigationBar = () => {
    return (
        <Navbar variant='dark'>
            <Container>
                <Navbar.Brand className="navLogoInputOTP">AyoBersuara.com</Navbar.Brand>
            </Container>
        </Navbar>
    )
}

export default NavigationBar;