import React from 'react';
import NavigationBar from "./NavbarHeader";
import Content from "./Content";
import "./AddFreeRoomStyle.css"

const AddFreeRoom = () => {
    return (
        <div>
            <div className="header">
                <NavigationBar/>
            </div>
            <div>
                <Content/>
            </div>
        </div>
    )
}

export default AddFreeRoom;