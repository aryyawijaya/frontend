import React from 'react';
import { Container, Navbar } from "react-bootstrap";
import "./loginStyle.css";

const NavigationBar = () => {
    return (
        <Navbar>
            <Container>
                <Navbar.Brand className="navLogo">AyoBersuara.com</Navbar.Brand>
            </Container>
        </Navbar>
    );
};

export default NavigationBar;