import React from 'react';
import './loginStyle.css';
import NavigationBar from './navbarHeader.js';
import Content from './Content.js';

const Login = () => {
    return (
        <div>
            <div className='header'>
                <NavigationBar/>
            </div>
            <div>
                <Content/>
            </div>
        </div>
    );
};

export default Login;