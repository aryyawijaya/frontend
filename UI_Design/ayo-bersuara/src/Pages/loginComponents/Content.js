import React from 'react';
import { useState, useEffect } from "react";
import "./loginStyle.css";
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import axios from "axios";
import { useNavigate } from "react-router-dom";

const Content = () => {
    const navigate = useNavigate();

    useEffect(() => {
        localStorage.removeItem("roomId");
        localStorage.removeItem("roomName");
        const id = localStorage.getItem("premiumRoomAdminId");
        if (id) {
            navigate("/administrator-dashboard");
            return;
        }
    }, []);

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const handleChangeEmail = (e) => {
        setEmail(e.target.value);
    };

    const handleChangePassword = (e) => {
        setPassword(e.target.value);
    };

    const onSubmitHandler = async (e) => {
        e.preventDefault();
        try {
            const result = await axios.post(
                "http://localhost:8001/premiumRoomAdmin/login",
                { email, password },
                {
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded",
                    },
                }
            );
            const id = result.data.Premium.premiumRoomAdminId;
            localStorage.setItem("premiumRoomAdminId", id);
            navigate("/administrator-dashboard");
        } catch (err) {
            console.log(err.response.data.message);
            alert(err.response.data.message);
        }
    };

    return (
        <div>
            <button 
            className = "backButtonLogin"
            onClick = {() => navigate("/dashboard")}>Back</button>
            <Row style={{padding: "0px", margin: "0px"}}>
                <Col>
                    <h1 className="text1Login">Login Untuk Membuat Room</h1>
                    <img className = "imgLogin"
                    src = "/asset/img1.png"
                    alt = ""/>
                </Col>
                <Col>
                    <div>
                        <form onSubmit={ onSubmitHandler }>
                            <label className="labelStyleLogin">Email</label>
                            <input
                                className="inputBoxLogin"
                                type="text"
                                name="email"
                                value={email}
                                onChange={ handleChangeEmail }
                            ></input>
                            <label className="labelStyleLogin">Password</label>
                            <input
                                className="inputBoxLogin"
                                type="text"
                                name="password"
                                value={password}
                                onChange={ handleChangePassword }
                            ></input>
                            <button type="submit" className="loginButtonLogin">Login</button>
                        </form>
                    </div>
                </Col>
            </Row>
        </div>
    );
};

export default Content;